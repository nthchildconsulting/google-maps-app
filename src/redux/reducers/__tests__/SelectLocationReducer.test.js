import SelectLocationReducer from '../SelectLocationReducer';
import * as types from '../../constants/ActionTypes';
import deepFreeze from 'deep-freeze';

describe('Locations Reducer', () => {
  it('should return the initial state', () => {
    expect(SelectLocationReducer(undefined, {})).toEqual(0);
  });
  it('should select a location', () => {
    const index = 1;
    const beforeState = 0;
    const afterState = 1;
    const action = {
      type: types.SELECT_LOCATION,
      index,
    };
    deepFreeze(beforeState);
    expect(SelectLocationReducer(beforeState, action)).toEqual(afterState);
  });
});
