import GoogleMapReducer from '../GoogleMapReducer';
import * as types from '../../constants/ActionTypes';

describe('Google Map Reducer', () => {
  it('should return the initial state', () => {
    expect(GoogleMapReducer(undefined, {})).toEqual(null);
  });
  it('should set the google map api key', () => {
    const key = 'test-key';
    const beforeState = null;
    const afterState = key;
    const action = { type: types.SET_GOOGLE_MAP_API_KEY, key };
    expect(GoogleMapReducer(beforeState, action)).toEqual(afterState);
  });
});
