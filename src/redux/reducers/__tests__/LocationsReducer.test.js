import LocationsReducer from '../LocationsReducer';
import * as types from '../../constants/ActionTypes';
import deepFreeze from 'deep-freeze';

const defaultLocation = {
  lat: 40.5437809,
  long: -111.816761,
};

describe('Locations Reducer', () => {
  it('should return the initial state', () => {
    expect(LocationsReducer(undefined, {})).toEqual([defaultLocation]);
  });
  it('should add a location', () => {
    const lat = 10;
    const long = 10;
    const beforeState = [defaultLocation];
    const afterState = [defaultLocation, { lat, long }];
    const action = {
      type: types.ADD_LOCATION,
      lat,
      long,
    };
    deepFreeze(beforeState);
    expect(LocationsReducer(beforeState, action)).toEqual(afterState);
  });
  it('should delete a location', () => {
    const index = 0;
    const lat = 10;
    const long = 10;
    const beforeState = [defaultLocation, { lat, long }];
    const afterState = [{ lat, long }];
    const action = {
      type: types.DELETE_LOCATION,
      index,
    };
    deepFreeze(beforeState);
    expect(LocationsReducer(beforeState, action)).toEqual(afterState);
  });
});
