import * as types from '../constants/ActionTypes';

const SelectLocationReducer = (state = 0, action) => {
  switch (action.type) {
    case types.SELECT_LOCATION:
      return action.index;
    default:
      return state;
  }
};

export default SelectLocationReducer;
