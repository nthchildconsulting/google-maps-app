import { combineReducers } from 'redux';
import LocationsReducer from './LocationsReducer';
import SelectLocationReducer from './SelectLocationReducer';
import GoogleMapReducer from './GoogleMapReducer';

export default combineReducers({
  locations: LocationsReducer,
  selectedLocationIndex: SelectLocationReducer,
  googleMapApiKey: GoogleMapReducer,
});
