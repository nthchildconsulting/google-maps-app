import * as types from '../constants/ActionTypes';

const defaultLocation = {
  lat: 40.5437809,
  long: -111.816761,
};

const LocationsReducer = (state = [defaultLocation], action) => {
  switch (action.type) {
    case types.ADD_LOCATION:
      return [
        ...state,
        {
          lat: action.lat,
          long: action.long,
        },
      ];
    case types.DELETE_LOCATION:
      return state.filter((item, index) => index !== action.index);
    default:
      return state;
  }
};

export default LocationsReducer;
