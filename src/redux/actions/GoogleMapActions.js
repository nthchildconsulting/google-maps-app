import * as types from '../constants/ActionTypes';

export const setGoogleMapApiKey = key => ({
  type: types.SET_GOOGLE_MAP_API_KEY,
  key,
});
