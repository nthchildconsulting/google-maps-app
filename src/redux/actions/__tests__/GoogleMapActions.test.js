import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../GoogleMapActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Google Map Actions', () => {
  it('should create an action to set the google map api key', () => {
    const key = 'test-key';
    const expectedActions = [
      {
        type: types.SET_GOOGLE_MAP_API_KEY,
        key,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.setGoogleMapApiKey(key));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
