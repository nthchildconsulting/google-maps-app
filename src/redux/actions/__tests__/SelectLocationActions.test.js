import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../SelectLocationActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Select Location Actions', () => {
  it('should create an action to select a location', () => {
    const index = 0;
    const expectedActions = [
      {
        type: types.SELECT_LOCATION,
        index,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.selectLocation(index));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
