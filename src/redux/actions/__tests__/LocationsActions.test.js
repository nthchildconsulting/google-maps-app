import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import * as actions from '../LocationsActions';
import * as types from '../../constants/ActionTypes';

const middlewares = [thunk];
const mockStore = configureMockStore(middlewares);

describe('Locations Actions', () => {
  it('should create an action to add a location', () => {
    const lat = 10;
    const long = 10;
    const expectedActions = [
      {
        type: types.ADD_LOCATION,
        lat,
        long,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.addLocation(lat, long));
    expect(store.getActions()).toEqual(expectedActions);
  });
  it('should create an action to delete a location', () => {
    const index = 0;
    const expectedActions = [
      {
        type: types.DELETE_LOCATION,
        index,
      },
    ];
    const store = mockStore([]);
    store.dispatch(actions.deleteLocation(index));
    expect(store.getActions()).toEqual(expectedActions);
  });
});
