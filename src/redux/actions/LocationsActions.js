import * as types from '../constants/ActionTypes';

export const addLocation = (lat, long) => ({
  type: types.ADD_LOCATION,
  lat,
  long,
});

export const deleteLocation = index => ({
  type: types.DELETE_LOCATION,
  index,
});
