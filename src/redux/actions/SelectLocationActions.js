import * as types from '../constants/ActionTypes';

export const selectLocation = index => ({
  type: types.SELECT_LOCATION,
  index,
});
