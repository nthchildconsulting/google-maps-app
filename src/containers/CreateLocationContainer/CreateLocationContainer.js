import { connect } from 'react-redux';
import { addLocation } from '../../redux/actions/LocationsActions';
import { selectLocation } from '../../redux/actions/SelectLocationActions';
import CreateLocation from '../../components/CreateLocation/CreateLocation';

const mapStateToProps = state => ({
  locations: state.locations,
});

const mapDispatchToProps = dispatch => ({
  addLocation: (lat, long) => dispatch(addLocation(lat, long)),
  selectLocation: index => dispatch(selectLocation(index)),
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(CreateLocation);
