import { connect } from 'react-redux';
import LocationsList from '../../components/LocationsList/LocationsList';
import { selectLocation } from '../../redux/actions/SelectLocationActions';
import { deleteLocation } from '../../redux/actions/LocationsActions';

const mapStateToProps = state => ({
  locations: state.locations,
  selectedLocationIndex: state.selectedLocationIndex,
});

const mapDispatchToProps = dispatch => ({
  onClick: index => dispatch(selectLocation(index)),
  onDelete: index => {
    dispatch(deleteLocation(index));
    dispatch(selectLocation(0));
  },
});

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(LocationsList);
