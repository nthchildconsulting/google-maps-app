import { connect } from 'react-redux';
import { setGoogleMapApiKey } from '../../redux/actions/GoogleMapActions';
import GoogleMapApiKey from '../../components/GoogleMapApiKey/GoogleMapApiKey';

const mapDispatchToProps = dispatch => ({
  setGoogleMapApiKey: key => dispatch(setGoogleMapApiKey(key)),
});

export default connect(
  null,
  mapDispatchToProps
)(GoogleMapApiKey);
