import { connect } from 'react-redux';
import App from '../../components/App/App';

const mapStateToProps = state => ({
  googleMapApiKey: state.googleMapApiKey,
});

export default connect(mapStateToProps)(App);
