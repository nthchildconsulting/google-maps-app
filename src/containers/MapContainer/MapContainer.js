import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { withStyles } from '@material-ui/core/styles';
import GoogleMap from '../../components/GoogleMap/GoogleMap';

const styles = theme => ({
  root: {
    marginBottom: '50px',
  },
});

class MapContainer extends Component {
  render() {
    const { classes, googleMapApiKey, locations } = this.props;
    const currentLocation = locations[this.props.selectedLocationIndex];
    if (!currentLocation) {
      return (
        <div className={classes.root}>Please add or select a location.</div>
      );
    }
    const { lat, long } = currentLocation;
    return (
      <div className={classes.root}>
        <GoogleMap lat={lat} long={long} googleMapApiKey={googleMapApiKey} />
      </div>
    );
  }
}

MapContainer.propTypes = {
  classes: PropTypes.object.isRequired,
  locations: PropTypes.array.isRequired,
  selectedLocationIndex: PropTypes.number.isRequired,
  googleMapApiKey: PropTypes.string.isRequired,
};

const mapStateToProps = state => ({
  locations: state.locations,
  selectedLocationIndex: state.selectedLocationIndex,
  googleMapApiKey: state.googleMapApiKey,
});

export default connect(mapStateToProps)(withStyles(styles)(MapContainer));
