import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Grid from '@material-ui/core/Grid';
import Divider from '@material-ui/core/Divider';
import Header from '../Header/Header';
import MapContainer from '../../containers/MapContainer/MapContainer';
import LocationsContainer from '../../containers/LocationsContainer/LocationsContainer';
import CreateLocationContainer from '../../containers/CreateLocationContainer/CreateLocationContainer';
import GoogleMapApiKeyContainer from '../../containers/GoogleMapApiKeyContainer/GoogleMapApiKeyContainer';

const styles = theme => ({
  root: {
    flexGrow: 1,
    backgroundColor: theme.palette.background.paper,
  },
  paper: {
    padding: theme.spacing.unit * 6,
    [theme.breakpoints.down('xs')]: {
      padding: theme.spacing.unit * 2,
    },
    textAlign: 'center',
    color: theme.palette.text.secondary,
  },
});

class App extends Component {
  render() {
    const { classes, googleMapApiKey } = this.props;
    return (
      <div className={classes.root}>
        <Grid container spacing={24} justify="center">
          <Grid item xs={12} sm={10} md={8}>
            <Paper className={classes.paper}>
              <Header />
              {!googleMapApiKey && <GoogleMapApiKeyContainer />}
              {googleMapApiKey && (
                <div>
                  <MapContainer />
                  <Divider />
                  <LocationsContainer />
                  <CreateLocationContainer />
                </div>
              )}
            </Paper>
          </Grid>
        </Grid>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  googleMapApiKey: PropTypes.string,
};

export default withStyles(styles)(App);
