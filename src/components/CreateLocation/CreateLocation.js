import React, { Component } from 'react';
import TextField from '@material-ui/core/TextField';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
    width: 200,
    [theme.breakpoints.down('xs')]: {
      width: '100%',
    },
  },
  button: {
    margin: theme.spacing.unit,
  },
  error: {
    color: 'red',
  },
});

class CreateLocation extends Component {
  constructor(props) {
    super(props);
    this.state = {
      lat: '',
      long: '',
      error: '',
    };
  }
  longValueChange(e) {
    this.setState({
      long: e.target.value,
    });
  }
  latValueChange(e) {
    this.setState({
      lat: e.target.value,
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    const { lat, long } = this.state;
    if (!Number(lat)) {
      this.setState({
        error: 'Latitude must be a number',
      });
      return;
    }
    if (!Number(long)) {
      this.setState({
        error: 'Longitude must be a number',
      });
      return;
    }
    this.props.addLocation(Number(lat), Number(long));
    const nextLocationIndex = this.props.locations.length;
    this.props.selectLocation(nextLocationIndex);
    this.setState({
      lat: '',
      long: '',
      error: '',
    });
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <TextField
            id="lat"
            label="Latitude"
            className={classes.textField}
            value={this.state.lat}
            onChange={this.latValueChange.bind(this)}
            margin="normal"
            required
          />
          <TextField
            id="long"
            label="Longitude"
            className={classes.textField}
            value={this.state.long}
            onChange={this.longValueChange.bind(this)}
            margin="normal"
            required
          />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            type="submit"
          >
            Add
          </Button>
        </form>
        {this.state.error && (
          <div className={classes.error}>{this.state.error}</div>
        )}
      </div>
    );
  }
}

CreateLocation.propTypes = {
  classes: PropTypes.object.isRequired,
  locations: PropTypes.array.isRequired,
};

export default withStyles(styles)(CreateLocation);
