import React from 'react';
import PropTypes from 'prop-types';
import Location from '../Location/Location';
import List from '@material-ui/core/List';

const LocationsList = ({
  locations,
  selectedLocationIndex,
  onClick,
  onDelete,
}) => (
  <List>
    {locations.map((location, index) => (
      <Location
        key={index}
        {...location}
        onClick={onClick}
        onDelete={onDelete}
        index={index}
        selected={index === selectedLocationIndex}
      />
    ))}
  </List>
);

LocationsList.propTypes = {
  locations: PropTypes.arrayOf(
    PropTypes.shape({
      lat: PropTypes.number.isRequired,
      long: PropTypes.number.isRequired,
    }).isRequired
  ).isRequired,
  selectedLocationIndex: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
};

export default LocationsList;
