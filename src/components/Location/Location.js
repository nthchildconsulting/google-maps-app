import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import IconButton from '@material-ui/core/IconButton';
import MapMarkerIcon from '@material-ui/icons/Map';
import DeleteIcon from '@material-ui/icons/Delete';

const styles = theme => ({
  root: {
    cursor: 'pointer',
    '&:hover': {
      backgroundColor: '#efefef',
    },
  },
  selected: {
    backgroundColor: '#efefef',
  },
});

const Location = ({
  lat,
  long,
  onClick,
  index,
  classes,
  selected,
  onDelete,
}) => {
  let className = '';
  if (selected) {
    className = classes.selected;
  }
  return (
    <ListItem
      className={className}
      classes={{ root: classes.root }}
      divider
      onClick={() => onClick(index)}
    >
      <ListItemIcon>
        <MapMarkerIcon />
      </ListItemIcon>
      <ListItemText primary={lat + ', ' + long} />
      <ListItemSecondaryAction>
        <IconButton aria-label="Delete" onClick={() => onDelete(index)}>
          <DeleteIcon />
        </IconButton>
      </ListItemSecondaryAction>
    </ListItem>
  );
};

Location.propTypes = {
  lat: PropTypes.number.isRequired,
  long: PropTypes.number.isRequired,
  onClick: PropTypes.func.isRequired,
  onDelete: PropTypes.func.isRequired,
  index: PropTypes.number.isRequired,
  selected: PropTypes.bool.isRequired,
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(Location);
