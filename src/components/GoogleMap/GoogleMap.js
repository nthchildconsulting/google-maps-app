import React from 'react';
import PropTypes from 'prop-types';
import {
  withScriptjs,
  withGoogleMap,
  GoogleMap,
  Marker,
} from 'react-google-maps';

const GoogleMapWithMarker = ({ lat, long, googleMapApiKey }) => {
  const GOOGLE_MAP_URL = `https://maps.googleapis.com/maps/api/js?key=${googleMapApiKey}&v=3.exp&libraries=geometry,drawing,places`;
  const MapWithAMarker = withScriptjs(
    withGoogleMap(({ lat, long }) => (
      <GoogleMap defaultZoom={12} defaultCenter={{ lat, lng: long }}>
        <Marker position={{ lat, lng: long }} />
      </GoogleMap>
    ))
  );
  return (
    <div>
      <MapWithAMarker
        googleMapURL={GOOGLE_MAP_URL}
        loadingElement={<div style={{ height: `100%` }} />}
        containerElement={<div style={{ height: `400px` }} />}
        mapElement={<div style={{ height: `100%` }} />}
        lat={lat}
        long={long}
      />
    </div>
  );
};

GoogleMapWithMarker.propTypes = {
  lat: PropTypes.number.isRequired,
  long: PropTypes.number.isRequired,
  googleMapApiKey: PropTypes.string.isRequired,
};

export default GoogleMapWithMarker;
