import React, { Component } from 'react';
import PropTypes from 'prop-types';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';

const styles = theme => ({
  textField: {
    marginLeft: theme.spacing.unit,
    marginRight: theme.spacing.unit,
  },
  button: {
    margin: theme.spacing.unit,
  },
});

class GoogleMapApiKey extends Component {
  constructor(props) {
    super(props);
    this.state = {
      key: '',
    };
  }
  handleChange(e) {
    this.setState({
      key: e.target.value,
    });
  }
  handleSubmit(e) {
    e.preventDefault();
    this.props.setGoogleMapApiKey(this.state.key);
  }
  render() {
    const { classes } = this.props;
    return (
      <div>
        <form onSubmit={this.handleSubmit.bind(this)}>
          <TextField
            id="key"
            label="Google Map API Key"
            className={classes.textField}
            value={this.state.key}
            onChange={this.handleChange.bind(this)}
            margin="normal"
            required
            fullWidth={true}
          />
          <Button
            variant="contained"
            color="primary"
            className={classes.button}
            type="submit"
          >
            Submit
          </Button>
        </form>
      </div>
    );
  }
}

GoogleMapApiKey.propTypes = {
  classes: PropTypes.object.isRequired,
  setGoogleMapApiKey: PropTypes.func.isRequired,
};

export default withStyles(styles)(GoogleMapApiKey);
